/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entite;

/**
 *
 * @author chedi
 */
public class Evenement {
    
    private int id_Ev;
    private String Nom_event;
    private String lien_Fb;
    private String date_debut;
    private String date_fin;
    private double prix_pass;
    private String description;
    private String list_contact;
    private String ville;
    private String cat_event;
    private String Email;
    private String Horaire;
    private int Places_Disponibles;
    private int id_organisateur;

    public Evenement() {
    }

    public Evenement(int id_Ev, String Nom_event, String lien_Fb, String date_debut, String date_fin, double prix_pass, String description, String list_contact, String ville, String cat_event, String Email, String Horaire, int Places_Disponibles, int id_organisateur) {
        this.id_Ev = id_Ev;
        this.Nom_event = Nom_event;
        this.lien_Fb = lien_Fb;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.prix_pass = prix_pass;
        this.description = description;
        this.list_contact = list_contact;
        this.ville = ville;
        this.cat_event = cat_event;
        this.Email = Email;
        this.Horaire = Horaire;
        this.Places_Disponibles = Places_Disponibles;
        this.id_organisateur = id_organisateur;
    }

    public int getId_Ev() {
        return id_Ev;
    }

    public void setId_Ev(int id_Ev) {
        this.id_Ev = id_Ev;
    }

    public String getNom_event() {
        return Nom_event;
    }

    public void setNom_event(String Nom_event) {
        this.Nom_event = Nom_event;
    }

    public String getLien_Fb() {
        return lien_Fb;
    }

    public void setLien_Fb(String lien_Fb) {
        this.lien_Fb = lien_Fb;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public double getPrix_pass() {
        return prix_pass;
    }

    public void setPrix_pass(double prix_pass) {
        this.prix_pass = prix_pass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getList_contact() {
        return list_contact;
    }

    public void setList_contact(String list_contact) {
        this.list_contact = list_contact;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCat_event() {
        return cat_event;
    }

    public void setCat_event(String cat_event) {
        this.cat_event = cat_event;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getHoraire() {
        return Horaire;
    }

    public void setHoraire(String Horaire) {
        this.Horaire = Horaire;
    }

    public int getPlaces_Disponibles() {
        return Places_Disponibles;
    }

    public void setPlaces_Disponibles(int Places_Disponibles) {
        this.Places_Disponibles = Places_Disponibles;
    }

    public int getId_organisateur() {
        return id_organisateur;
    }

    public void setId_organisateur(int id_organisateur) {
        this.id_organisateur = id_organisateur;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id_Ev=" + id_Ev + ", Nom_event=" + Nom_event + ", description=" + description + ", ville=" + ville + '}';
    }
    
    
}
