/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entite;

/**
 *
 * @author DELL
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;
import com.codename1.ui.List;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.MultiList;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.table.DefaultTableModel;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableModel;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.entite.CustomTableModel;
import com.mycompany.entite.reservation;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.codename1.messaging.Message;
import com.mycompany.myapp.ReservationForm;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author DELL
 */
public class ListReservationForm {
     Form fm ;
     public String id="";
  private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Déconnexion = new Command ("Déconnexion");
    public ArrayList<Map <String, Object>> data=new ArrayList<>();
 public ListReservationForm (Resources theme){
     UIBuilder uib = new UIBuilder();
        
       
     fm=new Form("vos réservations",new BorderLayout());
        int res1=Display.getInstance().convertToPixels(3);
        
        EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(res1 * 3, res1 * 4, 0), false);
        Image icon1 = URLImage.createToStorage(placeholder, "icon1", "http://localhost/mobile/31.jpg");
        
                   
             fm.getToolbar().addCommandToSideMenu(Acceuil);
                fm.getToolbar().addCommandToSideMenu(Vos_Reservations);
                fm.getToolbar().addCommandToSideMenu(Déconnexion);
        
            
                fm.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                 if (evt.getCommand()== Vos_Reservations){
              ListReservationForm reservation = new ListReservationForm(theme);
                 }
                 else if (evt.getCommand()== Acceuil){
             ReservationForm reservation = new ReservationForm(theme);
              reservation.getF().show();
                 }
            }
        });
        
           ConnectionRequest crliste=new ConnectionRequest()//
              { Map <String ,Object> data1;
       @Override 
      protected void postResponse() { 
          
          java.util.List<Map<String,Object>> content =(java.util.List<Map<String,Object>>)data1.get("root");
          
          for(Map<String,Object> obj :content){
              
                id= (String)obj.get("id_res")  ;
             
              data.add(createListEntry((String)obj.get("titre"), (String)obj.get("pays"), icon1));
              
            
          }
          
           DefaultListModel<Map<String, Object>> model = new DefaultListModel<>(data);
  MultiList ml = new MultiList(model);
  fm.add(BorderLayout.CENTER, ml);
  
  fm.show();
  
  
  

          
     ml.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent evt) {
          
                  
                              
                
                Form Mar = new Form();
                UIBuilder uib= new UIBuilder();
                
               
                Container co=new Container(new BoxLayout(BoxLayout.Y_AXIS));
                
                Mar.getToolbar().addCommandToSideMenu(Acceuil);
                Mar.getToolbar().addCommandToSideMenu(Vos_Reservations);
                Mar.getToolbar().addCommandToSideMenu(Déconnexion);
        
            
                Mar.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
              ListReservationForm reservation = new ListReservationForm(theme);
            }
        });
                
                ConnectionRequest crreservation=new ConnectionRequest()//
              { Map <String ,Object> data1;
       @Override 
      protected void postResponse() { 
          
          java.util.List<Map<String,Object>> content =(java.util.List<Map<String,Object>>)data1.get("root");
          
          for(Map<String,Object> obj :content){
             
                
              
               String [][]tabledata={{"id",(String)obj.get("id")},{"Civilité",(String)obj.get("civilite")},{"Nom",(String)obj.get("nom")},{"Prenom",(String)obj.get("prenom")}
               ,{"Adresse",(String)obj.get("Adresse")},{"Email",(String)obj.get("Email")},{"numtel",
                  (String)obj.get("numtel")},{"Date Expiration Carte",(String)obj.get("exp")},{"Type Carte",(String)obj.get("tpcarte")}};
                 CustomTableModel model = new CustomTableModel(new String [] {"Champs","Valeur"},tabledata);
                
               
                
                
                Table ta = new Table(model);
                
                
                
                
                
                
                
                
                Mar.add(ta);
                Mar.show();
              
            
          }
            
          
          
          
          
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
       JSONParser parser=new JSONParser(); 
       
       data1=parser.parseJSON(new InputStreamReader(input));
          
      
} };
         
                crreservation.setUrl("http://localhost/mobile/listerreservationspec.php");
                crreservation.addArgument("id_res",id );
                NetworkManager.getInstance().addToQueue(crreservation);
        
        
        
              
        
            
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
              }
          });
     
          
          
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
       JSONParser parser=new JSONParser(); 
       
       data1=parser.parseJSON(new InputStreamReader(input));
          
      
}       };
         
                crliste.setUrl("http://localhost/mobile/listerreservation.php");
                NetworkManager.getInstance().addToQueue(crliste);
        
        
        
             
                
                
                

        
        
 }

   

 
    public Form getF() {
        return fm;
    }

    public void setF(Form f) {
        this.fm = f;
    }
    
    
    
 public void show(){
     
      fm.show(); 
 }
  private Map<String, Object> createListEntry(String name, String date, Image icon) {
  Map<String, Object> entry = new HashMap<>();
  entry.put("Line1", name);
  entry.put("Line2", date);
  entry.put("icon", icon);
  return entry;
}
}


