/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entite;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.Date;

/**
 *
 * @author DELL
 */
public class reservation {
    
    private int id_res;
    private int id_log;
    private int nb_voyageur;
    private byte confirmation;
    private String num_tel;
    private String civilite;
    private String nom;
    private String prenom;
    private String email;
    private String pays;
    private String motdepasse;
    private String message;
    private String numcarte;
    private Date dateexpirationcarte;
    private Date expiredate;
    private int idProp;
    private String adresse;
    private String typecarte;

    public reservation( int id_log, int nb_voyageur, byte confirmation, String num_tel, String civilite, String nom, String prenom, String email, String pays, String motdepasse, String message, String numcarte, Date dateexpirationcarte, Date expiredate, int idProp, String adresse, String typecarte) {
        
        this.id_log = id_log;
        this.nb_voyageur = nb_voyageur;
        this.confirmation = confirmation;
        this.num_tel = num_tel;
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.pays = pays;
        this.motdepasse = motdepasse;
        this.message = message;
        this.numcarte = numcarte;
        this.dateexpirationcarte = dateexpirationcarte;
        this.expiredate = expiredate;
        this.idProp = idProp;
        this.adresse = adresse;
        this.typecarte = typecarte;
    }

    public reservation() {
    }
    
    
    
    

    public int getId_res() {
        return id_res;
    }

    public void setId_res(int id_res) {
        this.id_res = id_res;
    }

    public int getId_log() {
        return id_log;
    }

    public void setId_log(int id_log) {
        this.id_log = id_log;
    }

    public int getNb_voyageur() {
        return nb_voyageur;
    }

    public void setNb_voyageur(int nb_voyageur) {
        this.nb_voyageur = nb_voyageur;
    }

    public byte getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(byte confirmation) {
        this.confirmation = confirmation;
    }

    public String getNum_tel() {
        return num_tel;
    }

    public void setNum_tel(String num_tel) {
        this.num_tel = num_tel;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNumcarte() {
        return numcarte;
    }

    public void setNumcarte(String numcarte) {
        this.numcarte = numcarte;
    }

    public Date getDateexpirationcarte() {
        return dateexpirationcarte;
    }

    public void setDateexpirationcarte(Date dateexpirationcarte) {
        this.dateexpirationcarte = dateexpirationcarte;
    }

    public Date getExpiredate() {
        return expiredate;
    }

    public void setExpiredate(Date expiredate) {
        this.expiredate = expiredate;
    }

    public int getIdProp() {
        return idProp;
    }

    public void setIdProp(int idProp) {
        this.idProp = idProp;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTypecarte() {
        return typecarte;
    }

    public void setTypecarte(String typecarte) {
        this.typecarte = typecarte;
    }
    
    
    
     @Override
    public String toString() {
        return "reservation{" + "id=" + id_res + "id=" + id_log+ "id=" + idProp+ ", nom=" + nom + ", prenom=" + prenom + ", confirmation=" + confirmation+ ", numtel=" + num_tel+ ", civilité=" + civilite+ ", email=" + email+ ", pays=" + pays+ ", motdepasse=" + motdepasse+ ", message=" + message+ ", numcarte=" + numcarte+ ", date d expiration=" + dateexpirationcarte+ ", expiredate=" + expiredate+ ", adresse=" + adresse+ ", typecarte=" + typecarte+ ", nbrevoyageur=" + nb_voyageur + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

