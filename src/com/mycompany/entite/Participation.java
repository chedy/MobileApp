/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entite;



/**
 *
 * @author chedi
 */
public class Participation {
    private int id_p;
    private int id_ev ;
    private int id_user;
    private enum status{En_Attente, acceptee,refusee,annulee} ;
    private String date_participation;

    public Participation() {
    }

    public Participation(int id_p, int id_ev, int id_user, String dat_participation) {
        this.id_p = id_p;
        this.id_ev = id_ev;
        this.id_user = id_user;
        this.date_participation = dat_participation;
    }

    public Participation(int id_ev, int id_user, String date_participation) {
        this.id_ev = id_ev;
        this.id_user = id_user;
        this.date_participation = date_participation;
    }
    

    public int getId_p() {
        return id_p;
    }

    public void setId_p(int id_p) {
        this.id_p = id_p;
    }

    public int getId_ev() {
        return id_ev;
    }

    public void setId_ev(int id_ev) {
        this.id_ev = id_ev;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getDate_participation() {
        return date_participation;
    }

    public void setDate_participation(String dat_participation) {
        this.date_participation = dat_participation;
    }

    @Override
    public String toString() {
        return "Participation{" + "id_p= " + id_p + ", id_ev= " + id_ev + ", id_user= " + id_user + ", date_participation= " + date_participation + '}';
    }
         
    
}
