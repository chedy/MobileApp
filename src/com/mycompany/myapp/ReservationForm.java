/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;
import com.codename1.ui.List;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.MultiList;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.table.DefaultTableModel;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableModel;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.entite.CustomTableModel;
import com.mycompany.entite.reservation;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.codename1.messaging.Message;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author DELL
 */
public class ReservationForm {
     Form f ;
  public static reservation A = new reservation();
  private String rep;
    private int ch;
     private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Déconnexion = new Command ("Déconnexion");
 public ReservationForm (Resources theme){
     
    f = new Form("Réservation", BoxLayout.y());
            Container ct =new Container();
            SpanLabel spanLabel = new SpanLabel("civilité:");
            
            ComboBox CB1=new ComboBox("M","Mme");
            TextField TF1=new TextField("");
            TF1.setHint("Prénom");
            TextField TF2=new TextField("");
            TF2.setHint("Nom");
            TextField TF3=new TextField("");
            TF3.setHint("Adresse");
            TextField TF4=new TextField("");
            TF4.setHint("Email");
            TextField TF5=new TextField("");
            TF5.setHint("Nombre voyageurs");
            SpanLabel spanLabel1 = new SpanLabel("Pays:");
            ComboBox CB2=new ComboBox("Tunis","France");
            TextField TF6=new TextField("");
            TF6.setHint("Num-Tel");
            TextField TF7=new TextField("");
            TF7.setHint("Laissez un message");
            SpanLabel spanLabel2 = new SpanLabel("Type de votre carte");
            ComboBox CB3=new ComboBox("Visa","MasterCard","E-Dinar");
            SpanLabel spanLabel3 = new SpanLabel("Date d'expiration de votre carte");
            DateSpinner dt=new DateSpinner();
            
            TextField TF8=new TextField("");
            TF8.setHint("Num-Carte");
            
            TextField TF9=new TextField("");
            TF9.setHint("Mot de passe");
         
            Button BT=new Button("Reserver");
            f.add(spanLabel);
            f.add(CB1);
            f.add(TF1);
            f.add(TF2);
            f.add(TF3);
            f.add(TF4);
            f.add(TF5);
            f.add(spanLabel1);
            f.add(CB2);
            f.add(TF6);
            f.add(TF7);
            f.add(spanLabel2);
            f.add(CB3);
            f.add(spanLabel3);
            f.add(dt);
            f.add(TF8);
            f.add(TF9);
         
            f.add(BT);
            
            
            f.getToolbar().addCommandToSideMenu(Acceuil);
            f.getToolbar().addCommandToSideMenu(Vos_Reservations);
            f.getToolbar().addCommandToSideMenu(Déconnexion);
            
            f.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()== Vos_Reservations){
                       ListReservationForm reservation = new ListReservationForm(theme);
                    
                }
                  
                 //To change body of generated methods, choose Tools | Templates.
            }
        });
            
            
            
            
            
             BT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
               ConnectionRequest cr=new ConnectionRequest()//gérer les req
              { Map data;
       @Override 
      protected void postResponse() { 
          Dialog.show("",(String)data.get("reponse"), "Ok", null);
          
       ConfirmationForm reservation = new ConfirmationForm(theme);
       reservation.getF().show();
           
          
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
       JSONParser parser=new JSONParser(); 
       data=parser.parseJSON(new InputStreamReader(input));
         
} };
                
                
           
        
        A.setId_log(10);
        A.setIdProp(9);
        A.setAdresse(TF3.getText());
        A.setCivilite((String) CB1.getSelectedItem());
        A.setNom(TF2.getText());
        A.setPrenom(TF1.getText());
        A.setEmail( TF4.getText());
        A.setPays((String) CB2.getSelectedItem());
        A.setMotdepasse(TF9.getText());
        A.setMessage(TF7.getText());
        A.setNumcarte(TF8.getText());
        //A.setDateexpirationcarte("2017-04-30");
       //A.setExpiredate("2017-04-30");
        A.setTypecarte((String) CB3.getSelectedItem());
        A.setNb_voyageur(Integer.parseInt(TF5.getText()));
        Byte bt =(byte)0;
        A.setConfirmation(bt);
        A.setNum_tel(TF6.getText());
        
        
        
        
        
              cr.setUrl("http://localhost/mobile/ajouterreservation.php");
              cr.addArgument("id_log", "10");
                
              cr.addArgument("nb_voyageur", TF5.getText());
              cr.addArgument("confirmation", "0");
              
              cr.addArgument("num_tel", TF6.getText());
              
              cr.addArgument("civilite",(String) CB1.getSelectedItem());  
            
              cr.addArgument("nom", TF2.getText()); 
               
              cr.addArgument("prenom", TF1.getText());  
               
              cr.addArgument("email", TF4.getText());
            
              cr.addArgument("pays", (String) CB2.getSelectedItem());
               
              cr.addArgument("motdepasse", TF9.getText());  
                 
              cr.addArgument("message", TF7.getText());
                 
              cr.addArgument("numcarte", TF8.getText());  
              cr.addArgument("dateexpirationcarte", "2017-04-25");
              cr.addArgument("expiredate", "2017-04-25");
              cr.addArgument("idProp", "9");
                 
              cr.addArgument("adresse", TF3.getText());
                 
              cr.addArgument("typecarte", (String) CB3.getSelectedItem());
               NetworkManager.getInstance().addToQueue(cr);//executer les req
               InfiniteProgress prog=new  InfiniteProgress();
               Dialog d = prog.showInifiniteBlocking();
               cr.setDisposeOnCompletion(d);
                StringBuffer str = new StringBuffer();
                       ConnectionRequest crres=new ConnectionRequest()//gérer les req
              { Map data;
       @Override 
      protected void postResponse() { 
          A.setId_res(Integer.parseInt(rep));
         
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
           rep="";
     while ((ch=input.read())!=-1) {
                    str.append((char)ch);
                  
                }
                rep=str.toString();
      
} };
                       crres.setUrl("http://localhost/mobile/RecupererRes.php");
                NetworkManager.getInstance().addToQueue(crres);
                
              
            }
        });
     
 
 }

   

 
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
 public void show(){
     
      f.show(); 
 }
}
        