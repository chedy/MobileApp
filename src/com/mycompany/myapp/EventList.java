/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.DateFormat;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.entite.Evenement;
import java.io.IOException;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chedi
 */
public class EventList extends Form {
    
    private Resources theme;
    private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Events = new Command ("Evènements");
    private Command MyEvents = new Command ("Mes évènements");
    private Command Déconnexion = new Command ("Déconnexion");
    private Command Ajout = new Command ("Add Event");
    private Command modif = new Command ("Update Event");
    private Command stats = new Command("Top Events") ;
//    public static final String link = "http://localhost/WebPifinal(chedi)/WebPi/web/app_dev.php/EventList";
    int id;
    public void getF(Resources res)
    {
        Form f = new Form("Event List", BoxLayout.y());
        Toolbar toolbar=new Toolbar();
        f.setToolBar(toolbar);
        f.getToolbar().addCommandToSideMenu(Acceuil);
        f.getToolbar().addCommandToSideMenu(Vos_Reservations);
        f.getToolbar().addCommandToSideMenu(Events);
        f.getToolbar().addCommandToSideMenu(MyEvents);
        f.getToolbar().addCommandToSideMenu(Déconnexion);
        f.getToolbar().addCommandToOverflowMenu(Ajout);
        f.getToolbar().addCommandToOverflowMenu(modif);
        f.getToolbar().addCommandToOverflowMenu(stats);
        
        f.show();
        ConnectionRequest con= new ConnectionRequest();
        con.setUrl("http://127.0.0.1/piwebservice1/select_evt.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt)
            {       
                  System.out.println(getListEvent(new String(con.getResponseData())));
                  ArrayList<Evenement> list = getListEvent(new String(con.getResponseData()));
                  for (Evenement e : list) {
//                    SpanLabel sp = new SpanLabel();
//                    f.add(sp);
//                    sp.setText(e.toString());
//                    System.out.println("hayyyyaaaaaaaaaa");
//                    System.out.println(e.toString());
                    Container contAff= new Container(new FlowLayout(CENTER));
                            
                  contAff.addComponent(getContainer(e.getPlaces_Disponibles(), e.getNom_event(),res));
                  f.add(contAff);
                  Button det= new Button("Détails");
                  f.add(det);
                      
                  det.addActionListener(new ActionListener() 
                  {
                      @Override
                      public void actionPerformed(ActionEvent evt)
                      {
                          Form f1 = new Form("Event Détails", BoxLayout.y());
                          Toolbar toolbar=new Toolbar();
                          f1.setToolBar(toolbar);
                          f1.getToolbar().addCommandToSideMenu(Acceuil);
                          f1.getToolbar().addCommandToSideMenu(Vos_Reservations);
                          f1.getToolbar().addCommandToSideMenu(Events);
                          f1.getToolbar().addCommandToSideMenu(MyEvents);
                          f1.getToolbar().addCommandToSideMenu(Déconnexion);
                          f1.getToolbar().addCommandToOverflowMenu(Ajout);
                          f1.getToolbar().addCommandToOverflowMenu(modif);
                          f1.getToolbar().addCommandToOverflowMenu(stats);
                          f1.show();
                           // Image image= res.getImage("large (16).jpg");
                            //Label img= new Label(image);
                          SpanLabel NomEvent= new SpanLabel("Nom Evenement: "+e.getNom_event());
                          SpanLabel CatEvent= new SpanLabel("Catégorie: "+e.getCat_event());
                          SpanLabel Ville= new SpanLabel("Ville: "+e.getVille());
                          SpanLabel desc= new SpanLabel("Description: "+e.getDescription());
                          SpanLabel Prix= new SpanLabel("Prix: "+e.getPrix_pass());
                          SpanLabel Places= new SpanLabel("Places disponibles: "+e.getPlaces_Disponibles());
                          SpanLabel Mail= new SpanLabel("Mail: "+e.getEmail());
                          SpanLabel span1 = new SpanLabel();
                          SpanLabel span2 = new SpanLabel();
                          SpanLabel span3 = new SpanLabel();
                          SpanLabel span4 = new SpanLabel();
                          SpanLabel span5 = new SpanLabel();
                          SpanLabel span6 = new SpanLabel();
                          SpanLabel span7 = new SpanLabel();
                          Image img1 = res.getImage("Event Accepted-48.png");
                          Image img2 = res.getImage("Category-48.png");
                          Image img3 = res.getImage("Google Maps-48.png");
                          Image img4 = res.getImage("Literature-64.png");
                          Image img5 = res.getImage("Cash in Hand-48.png");
                          Image img6 = res.getImage("User Groups-50.png");
                          Image img7 = res.getImage("Email-64.png");
                          span1.setIcon(img1);
                          span2.setIcon(img2);
                          span3.setIcon(img3);
                          span4.setIcon(img4);
                          span5.setIcon(img5);
                          span6.setIcon(img6);
                          span7.setIcon(img7);
                          Button partic= new Button("Participer");
                          Button Annulpart= new Button("Annuler Part");
                          Button DelEvent= new Button("Supprimer");
                          Container contInfo=new Container(new GridLayout(7,2));
                          Container contbtn=new Container(new FlowLayout(CENTER));
                          contbtn.add(partic);
                          contbtn.add(Annulpart);
                          Curentuser user= new Curentuser();
                          int id_user=user.getCurrentId();
                          if(e.getId_organisateur()==id_user){contbtn.add(DelEvent);}
//                          f1.add(img);
                          contInfo.add(span1);
                          contInfo.add(NomEvent);
                          contInfo.add(span2);
                          contInfo.add(CatEvent);
                          contInfo.add(span3);
                          contInfo.add(Ville);
                          contInfo.add(span4);
                          contInfo.add(desc);
                          contInfo.add(span5);
                          contInfo.add(Prix);
                          contInfo.add(span6);
                          contInfo.add(Places);
                          contInfo.add(span7);
                          contInfo.add(Mail);
                          f1.add(contInfo);
                          f1.add(contbtn);
                          
                          int id_ev= e.getId_Ev();
                          String status="En_Attente";
//                        Date date=new Date();
//                        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                          String date_part= "2017-01-05 21:20:30";
//                        f1.add(partic);



                            //Effectuer la participation**************************
                            
                            
                           partic.addActionListener(new ActionListener() 
                           {
                                @Override
                                public void actionPerformed(ActionEvent evt) 
                                {
                                    
                                    ConnectionRequest cnx= new ConnectionRequest();
                                    cnx.setUrl("http://localhost/piwebservice1/insert_part.php?id_ev="+id_ev+
                                              "&id_user="+id_user+"&status="+status+"&date_participation="+date_part);
                                    cnx.addResponseListener(new ActionListener<NetworkEvent>() 
                                    {
                                        @Override
                                        public void actionPerformed(NetworkEvent evt) 
                                        {
                                            String response = new String( cnx.getResponseData());
                                              
       
                                            if (response .equals("success"))
                                            {
                                                
                                              Dialog.show("Participation", " Votre participation a été effectuée avec succés ", "OK", "CANCEL");
                                                //Modification des places après participation***************
                                                //*****************************
                                                
                                              ConnectionRequest cr= new ConnectionRequest();
                                              cr.setUrl("http://localhost/piwebservice1/SetPlace_event.php?id_Ev="+id_ev);
                                              cr.addResponseListener(new ActionListener<NetworkEvent>() 
                                              {
                                                @Override
                                                public void actionPerformed(NetworkEvent evt) 
                                                {
                                                    String resp = new String( cr.getResponseData());
                                                    if (response .equals("success")){
                                                    Dialog.show("Places", " Nombre de places modifié", "OK", "CANCEL");}
                                                    else Dialog.show("Error", " Nombre de places n'a pas été modifié", "OK", "CANCEL");
                                                }
                                              });
                                              NetworkManager.getInstance().addToQueue(cr);
                                              EventList ev= new EventList();
                                              ev.getF(theme);
                                            }
                                              else Dialog.show("Error", " Failed ¨participation","Ok","Cancel");
                                        }
                                    });
                                    NetworkManager.getInstance().addToQueue(cnx);
                                }
                            });
                              
                              
                              //Annulation de la participation*************************
                              
                              Annulpart.addActionListener(new ActionListener() {
                                  @Override
                                  public void actionPerformed(ActionEvent evt) 
                                  {
                                     ConnectionRequest annulreq= new ConnectionRequest();
                                     annulreq.setUrl("http://localhost/piwebservice1/Annul_part.php?id_ev="+id_ev);
                                     annulreq.addResponseListener(new ActionListener<NetworkEvent>() {
                                         @Override
                                         public void actionPerformed(NetworkEvent evt) {
                                            String respAnnulation = new String( annulreq.getResponseData());
                                        if (respAnnulation .equals("success"))
                                        {
                                        Dialog.show("Annulation", " Participation Annulée", "OK", "CANCEL");
                                        //Modification des places après annulation de participation
                                        
                                     ConnectionRequest req= new ConnectionRequest();
                                     req.setUrl("http://localhost/piwebservice1/Places_decrease.php?id_Ev="+id_ev);
                                     req.addResponseListener(new ActionListener<NetworkEvent>() {
                                     @Override
                                     public void actionPerformed(NetworkEvent evt) 
                                        {
                                        String respAnnul = new String( req.getResponseData());
                                        if (respAnnul .equals("success")){
                                        Dialog.show("Places", " Nombre de places modifié", "OK", "CANCEL");}
                                        else Dialog.show("Error", " Nombre de places n'a pas été modifié", "OK", "CANCEL");
                                        
                                        }
                                        });
                                NetworkManager.getInstance().addToQueue(req);
                                EventList ev= new EventList();
                                ev.getF(theme);
                                }
                                else Dialog.show("Error", " Failed Annulation","Ok","Cancel");
                                         
                                }
                                });
                                     
                                NetworkManager.getInstance().addToQueue(annulreq);     
                                     
                                  }
                              });
            //Suppression d'évènement ***********************
            
            DelEvent.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt)
                {
                    ConnectionRequest annulreq= new ConnectionRequest();
                    annulreq.setUrl("http://localhost/piwebservice1/Annul_part.php?id_ev="+id_ev);
                    annulreq.addResponseListener(new ActionListener<NetworkEvent>() 
                    {
                        @Override
                        public void actionPerformed(NetworkEvent evt) 
                        {
                            String respAnnulation = new String( annulreq.getResponseData());
                            if (respAnnulation .equals("success"))
                            {           
                            Dialog.show("Annulation", " toutes les Participations de cet evenement sont Annulées", "OK", "CANCEL");
                            ConnectionRequest Del_req= new ConnectionRequest();
                            Del_req.setUrl("http://localhost/piwebservice1/Delete_event.php?id_Ev="+id_ev);
                            Del_req.addResponseListener(new ActionListener<NetworkEvent>() {
                                @Override
                                public void actionPerformed(NetworkEvent evt) 
                                {
                                    String r= new String(Del_req.getResponseData());
                                    if(r.equals("success"))
                                       Dialog.show("Suppression", "Evenement supprimé avec succés","Ok","Cancel");
                                    else Dialog.show("Error", " Failed Suppression","Ok","Cancel");

                                }
                            });
                            NetworkManager.getInstance().addToQueue(Del_req);
                    
                            EventList ev= new EventList();
                            ev.getF(theme);
                                    }         
                            else Dialog.show("Error", " Failed Annulation","Ok","Cancel");
                                         
                        }
                    });
                    NetworkManager.getInstance().addToQueue(annulreq);
                                      
                }
                });
                              
                              
                              //Traitement selon la commande séectionnée*******
                              
                              f1.addCommandListener(e->
                              {
                                  if(e.getCommand()==Events)
                                  {
                                      EventList ev= new EventList();
                                      ev.getF(theme);
                                  }
                                  else if(e.getCommand()==Acceuil)
                                  {
                                      logementForm log= new logementForm(theme);
                                      log.getL().show();
                                  }
                                  else if(e.getCommand()==Vos_Reservations)
                                  {
                                      ListReservationForm res= new ListReservationForm(theme);
                                      res.show();
                                  }
                                  else if(e.getCommand()==Déconnexion)
                                  {
                                      login lgn= new login(theme);
                                      lgn.show();
                                  }
                                  else if(e.getCommand()==stats)
                                  {
                                      Stats stat= new Stats();
                                      stat.createPieChartForm(theme).show();
                                  }
                                  else if(e.getCommand()==MyEvents)
                                  {
                                      MesEvenements MyEv= new MesEvenements();
                                      MyEv.getForm(theme);
                                  }
                              });
                              
                              
                              
                              
                              
                              
                              
                          }
                      });
                      
                  }
                  f.refreshTheme();
            }
                  
                  
                  });
        NetworkManager.getInstance().addToQueue(con);
        f.addCommandListener(e->
                              {
                                  if(e.getCommand()==Events)
                                  {
                                      EventList ev= new EventList();
                                      ev.getF(theme);
                                  }
                                  else if(e.getCommand()==Acceuil)
                                  {
                                      logementForm log= new logementForm(theme);
                                      log.getL().show();
                                  }
                                  else if(e.getCommand()==Vos_Reservations)
                                  {
                                      ListReservationForm reserv= new ListReservationForm(theme);
                                      reserv.show();
                                  }
                                  else if(e.getCommand()==Déconnexion)
                                  {
                                      login lgn= new login(theme);
                                      lgn.show();
                                  }
                                  else if(e.getCommand()==stats)
                                  {
                                      Stats stat= new Stats();
                                      stat.createPieChartForm(theme).show();
                                  }
                                  else if(e.getCommand()==MyEvents)
                                  {
                                      MesEvenements MyEv= new MesEvenements();
                                      MyEv.getForm(theme);
                                      
                                      
                                  }
                              });
        
        
                
    }
    public ArrayList<Evenement> getListEvent(String json) {
        ArrayList<Evenement> listevent = new ArrayList<>();
        
        try{
            JSONParser j = new JSONParser();
        
            Map<String, Object> events = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) events.get("evenement");
            for (Map<String, Object> obj : list) {
                Evenement e = new Evenement();
                e.setId_Ev(Integer.parseInt(obj.get("id_event").toString()));
                e.setNom_event(obj.get("Nom_event").toString());
                e.setVille(obj.get("ville").toString());
                e.setDescription(obj.get("description").toString());
                e.setPlaces_Disponibles(Integer.parseInt(obj.get("Places_disponibles").toString()));
                e.setEmail(obj.get("Email").toString());
                e.setList_contact(obj.get("list_contact").toString());
                e.setLien_Fb(obj.get("lien_Fb").toString());
                e.setDate_debut(obj.get("date_debut").toString());
                e.setDate_fin(obj.get("date_fin").toString());
                e.setHoraire(obj.get("horaire").toString());
                e.setPrix_pass(Double.parseDouble(obj.get("prix_pass").toString()));
                e.setCat_event(obj.get("cat_event").toString());
                listevent.add(e);

            }
            
        }
        catch(IOException e){}
        return listevent;

    }
    
    
    public Container getContainer (int id, String desc,Resources res)
    {
        Container cont = new Container(new BorderLayout());
        Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        ct.addComponent(new Label(id+""));
        ct.addComponent(new Label(desc+""));
        cont.add(BorderLayout.CENTER,ct);
      // Image image= res.getImage("large (16).jpg");
        Label imageLab= new Label();
        cont.addComponent(BorderLayout.WEST,imageLab);
       
        
       return cont; 
    
    }
    
}
