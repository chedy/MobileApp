/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author chedi
 */
public class Stats {
    
    private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Events = new Command ("Evènements");
    private Command Déconnexion = new Command ("Déconnexion");
    private Command Ajout = new Command ("Add Event");
    private Command modif = new Command ("Update Event");
    private Command stats = new Command("Top Events") ;
    
   
        private DefaultRenderer buildCategoryRenderer(int[] colors) {
    DefaultRenderer renderer = new DefaultRenderer();
    renderer.setLabelsTextSize(15);
    renderer.setLegendTextSize(15);
    renderer.setMargins(new int[]{20, 30, 15, 0});
    for (int color : colors) {
        SimpleSeriesRenderer r = new SimpleSeriesRenderer();
        r.setColor(color);
        renderer.addSeriesRenderer(r);
    }
    return renderer;
}

/**
 * Builds a category series using the provided values.
 *
 * @param titles the series titles
 * @param values the values
 * @return the category series
 */
protected CategorySeries buildCategoryDataset(String title, double[] values) {
    CategorySeries series = new CategorySeries(title);
    int k = 0;
    for (double value : values) {
        series.add("Event " + ++k, value);
    }

    return series;
}

public Form createPieChartForm(Resources theme) {
    // Generate the values
    double[] values = new double[]{12, 14, 11, 10, 19};

    // Set up the renderer
    int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA, ColorUtil.YELLOW, ColorUtil.CYAN};
    DefaultRenderer renderer = buildCategoryRenderer(colors);
    renderer.setZoomButtonsVisible(true);
    renderer.setZoomEnabled(true);
    renderer.setChartTitleTextSize(20);
    renderer.setDisplayValues(true);
    renderer.setShowLabels(true);
    SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);
    r.setGradientEnabled(true);
    r.setGradientStart(0, ColorUtil.BLUE);
    r.setGradientStop(0, ColorUtil.GREEN);
    r.setHighlighted(true);

    // Create the chart ... pass the values and renderer to the chart object.
    PieChart chart = new PieChart(buildCategoryDataset("Participants aux évènements", values), renderer);

    // Wrap the chart in a Component so we can add it to a form
    ChartComponent c = new ChartComponent(chart);

    // Create a form and show it.
    
    Form f = new Form("Budget", new BorderLayout());
    Toolbar toolbar=new Toolbar();
        f.setToolBar(toolbar);
        f.getToolbar().addCommandToSideMenu(Acceuil);
        f.getToolbar().addCommandToSideMenu(Vos_Reservations);
        f.getToolbar().addCommandToSideMenu(Events);
        f.getToolbar().addCommandToSideMenu(Déconnexion);
        f.getToolbar().addCommandToOverflowMenu(Ajout);
        f.getToolbar().addCommandToOverflowMenu(modif);
        f.getToolbar().addCommandToOverflowMenu(stats);
        f.addCommandListener(e->
                              {
                                  if(e.getCommand()==Events)
                                  {
                                      EventList ev= new EventList();
                                      ev.getF(theme);
                                  }
                                  else if(e.getCommand()==Acceuil)
                                  {
                                      logementForm log= new logementForm(theme);
                                      log.getL().show();
                                  }
                                  else if(e.getCommand()==Vos_Reservations)
                                  {
                                      ListReservationForm res= new ListReservationForm(theme);
                                      res.show();
                                  }
                                  else if(e.getCommand()==Déconnexion)
                                  {
                                      login lgn= new login(theme);
                                      lgn.show();
                                  }
                                  else if(e.getCommand()==stats)
                                  {
                                      Stats stat= new Stats();
                                      stat.createPieChartForm(theme).show();
                                  }
                              });
    f.add(BorderLayout.CENTER, c);
    return f;

}
    }
    

