/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.entite.Evenement;
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chedi
 */
public class MesEvenements {
    
    
    private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Events = new Command ("Evènements");
    private Command Déconnexion = new Command ("Déconnexion");
    private Command Ajout = new Command ("Add Event");
    private Command modif = new Command ("Update Event");
    private Command stats = new Command("Top Events") ;
    
    
    public void getForm(Resources theme)
    {
        Form f = new Form("My Events List", BoxLayout.y());
        Toolbar toolbar=new Toolbar();
        f.setToolBar(toolbar);
        f.getToolbar().addCommandToSideMenu(Acceuil);
        f.getToolbar().addCommandToSideMenu(Vos_Reservations);
        f.getToolbar().addCommandToSideMenu(Events);
        
        f.getToolbar().addCommandToSideMenu(Déconnexion);
        f.getToolbar().addCommandToOverflowMenu(Ajout);
        f.getToolbar().addCommandToOverflowMenu(modif);
        f.getToolbar().addCommandToOverflowMenu(stats);
        
        f.show();
        ConnectionRequest con= new ConnectionRequest();
        Curentuser user= new Curentuser();
        int id_user=user.getCurrentId();
        con.setUrl("http://localhost/piwebservice1/MesEvenements.php?id_organisateur="+id_user);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                
                ArrayList<Evenement> list = getListEvent(new String(con.getResponseData()));
                for (Evenement e : list) {
//                    SpanLabel sp = new SpanLabel();
//                    f.add(sp);
//                    sp.setText(e.toString());
//                    System.out.println("hayyyyaaaaaaaaaa");
//                    System.out.println(e.toString());
                  f.addComponent(getContainer(e.getPlaces_Disponibles(), e.getNom_event()));
                  Button det= new Button("Détails");
                  f.add(det);
                  f.show();
                      
                  det.addActionListener(new ActionListener() 
                  {
                      @Override
                      public void actionPerformed(ActionEvent evt)
                      {
                          Form f1 = new Form("Event Détails", BoxLayout.y());
                          Toolbar toolbar=new Toolbar();
                          f1.setToolBar(toolbar);
                          f1.getToolbar().addCommandToSideMenu(Acceuil);
                          f1.getToolbar().addCommandToSideMenu(Vos_Reservations);
                          f1.getToolbar().addCommandToSideMenu(Events);
                          
                          f1.getToolbar().addCommandToSideMenu(Déconnexion);
                          f1.getToolbar().addCommandToOverflowMenu(Ajout);
                          f1.getToolbar().addCommandToOverflowMenu(modif);
                          f1.getToolbar().addCommandToOverflowMenu(stats);
                          f1.show();
//                              Image image= res.getImage("large (16).jpg");
//                              Label img= new Label(image);
                          SpanLabel NomEvent= new SpanLabel("Nom Evenement: "+e.getNom_event());
                          SpanLabel CatEvent= new SpanLabel("Catégorie: "+e.getCat_event());
                          SpanLabel Ville= new SpanLabel("Ville: "+e.getVille());
                          SpanLabel desc= new SpanLabel("Description: "+e.getDescription());
                          SpanLabel Prix= new SpanLabel("Prix: "+e.getPrix_pass());
                          SpanLabel Places= new SpanLabel("Places disponibles: "+e.getPlaces_Disponibles());
                          SpanLabel Mail= new SpanLabel("Mail: "+e.getEmail());
                          Button partic= new Button("Participer");
                          Button Annulpart= new Button("Annuler Part");
                          Button DelEvent= new Button("Supprimer");
                          Container contbtn=new Container(new FlowLayout(CENTER));
                          contbtn.add(partic);
                          contbtn.add(Annulpart);
                          contbtn.add(DelEvent);
//                        contbtn.add(img);
                          f1.add(NomEvent);
                          f1.add(CatEvent);
                          f1.add(Ville);
                          f1.add(desc);
                          f1.add(Prix);
                          f1.add(Places);
                          f1.add(Mail);
                          f1.add(contbtn);
                          f1.show();
                          
                          f1.addCommandListener(e->
                              {
                                  if(e.getCommand()==Events)
                                  {
                                      EventList ev= new EventList();
                                      ev.getF(theme);
                                  }
                                  else if(e.getCommand()==Acceuil)
                                  {
                                      logementForm log= new logementForm(theme);
                                      log.getL().show();
                                  }
                                  else if(e.getCommand()==Vos_Reservations)
                                  {
                                      ListReservationForm res= new ListReservationForm(theme);
                                      res.show();
                                  }
                                  else if(e.getCommand()==Déconnexion)
                                  {
                                      login lgn= new login(theme);
                                      lgn.show();
                                  }
                                  else if(e.getCommand()==stats)
                                  {
                                      Stats stat= new Stats();
                                      stat.createPieChartForm(theme).show();
                                  }
                                 
                              });
                        f.refreshTheme();      
                      }
                  });
                    
                    f.addCommandListener(er->
                              {
                                  if(er.getCommand()==Events)
                                  {
                                      EventList ev= new EventList();
                                      ev.getF(theme);
                                  }
                                  else if(er.getCommand()==Acceuil)
                                  {
                                      logementForm log= new logementForm(theme);
                                      log.getL().show();
                                  }
                                  
                                  else if(er.getCommand()==Vos_Reservations)
                                  {
                                      ListReservationForm res= new ListReservationForm(theme);
                                      res.show();
                                  }
                                  else if(er.getCommand()==Déconnexion)
                                  {
                                      login lgn= new login(theme);
                                      lgn.show();
                                  }
                                  else if(er.getCommand()==stats)
                                  {
                                      Stats stat= new Stats();
                                      stat.createPieChartForm(theme).show();
                                  }
                                 
                              });
                    
                }
                 
            }
        });
        NetworkManager.getInstance().addToQueue(con);
                    f.refreshTheme();
    }
    
    
    public ArrayList<Evenement> getListEvent(String json) {
        ArrayList<Evenement> listevent = new ArrayList<>();
        
        try{
            JSONParser j = new JSONParser();
        
            Map<String, Object> events = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) events.get("evenement");
            for (Map<String, Object> obj : list) {
                Evenement e = new Evenement();
                e.setId_Ev(Integer.parseInt(obj.get("id_event").toString()));
                e.setNom_event(obj.get("Nom_event").toString());
                e.setVille(obj.get("ville").toString());
                e.setDescription(obj.get("description").toString());
                e.setPlaces_Disponibles(Integer.parseInt(obj.get("Places_disponibles").toString()));
                e.setEmail(obj.get("Email").toString());
                e.setList_contact(obj.get("list_contact").toString());
                e.setLien_Fb(obj.get("lien_Fb").toString());
                e.setDate_debut(obj.get("date_debut").toString());
                e.setDate_fin(obj.get("date_fin").toString());
                e.setHoraire(obj.get("horaire").toString());
                e.setPrix_pass(Double.parseDouble(obj.get("prix_pass").toString()));
                e.setCat_event(obj.get("cat_event").toString());
                listevent.add(e);

            }
            
        }
        catch(IOException e){}
        return listevent;

    }
    public Container getContainer (int id, String desc)
    {
        Container cont = new Container(new BorderLayout());
        Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        ct.addComponent(new Label(id+""));
        ct.addComponent(new Label(desc+""));
        cont.add(BorderLayout.CENTER,ct);
       
        Label imageLab= new Label();
        cont.addComponent(BorderLayout.WEST,imageLab);
        
        
           
        
       return cont; 
    
    }
    
    
}
