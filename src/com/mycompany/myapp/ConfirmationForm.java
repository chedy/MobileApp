/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

/**
 *
 * @author DELL
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;
import com.codename1.ui.List;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.MultiList;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.table.DefaultTableModel;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableModel;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.entite.reservation;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.codename1.messaging.Message;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author DELL
 */
public class ConfirmationForm {
     Form fm ;
  private Command Acceuil = new Command("Acceuil") ;
    private Command Vos_Reservations = new Command ("vos reservations");
    private Command Déconnexion = new Command ("Déconnexion");
 public ConfirmationForm (Resources theme){
     
    UIBuilder uib = new UIBuilder();
    fm=new Form("", BoxLayout.y());
    Container ct15 =new Container();
     SpanLabel LA1 = new SpanLabel("numcarte");
     
     SpanLabel LA2 = new SpanLabel("adresseEmail");
     SpanLabel LA3 = new SpanLabel("numTel");
     SpanLabel LA4 = new SpanLabel("pays");
     SpanLabel LA5 = new SpanLabel("NbVoyageurs");
     SpanLabel LA6 = new SpanLabel("adresse");
     SpanLabel LA7 = new SpanLabel("typeCarte");
     SpanLabel LA8 = new SpanLabel("civilité");
     Button BT3=new Button ("Confirmer votre réservation");
    
          fm.add(LA7);
          fm.add(LA6);
          fm.add(LA2);
          fm.add(LA4);
          fm.add(LA3);
          fm.add(LA5);
          fm.add(LA8);
          fm.add(LA1);
         
         
         
     
         
        
        
         fm.add(BT3);
         fm.getToolbar().addCommandToSideMenu(Acceuil);
         fm.getToolbar().addCommandToSideMenu(Vos_Reservations);
         fm.getToolbar().addCommandToSideMenu(Déconnexion);
        
        
       LA7.setText(""+ReservationForm.A.getCivilite()+" "+ReservationForm.A.getNom()+" "+ReservationForm.A.getPrenom()+" vous avez reservé l'appartement à turquie");
        LA6.setText("Adresse: "+ReservationForm.A.getAdresse());
         LA2.setText("AdresseEmail: "+ReservationForm.A.getEmail());
          LA4.setText("Pays: "+ReservationForm.A.getPays());
        
        
        
       
        
        
        LA3.setText("NuméroTel: "+ReservationForm.A.getNum_tel());
        
        
       
        
       
        LA5.setText("Nombre de voyageurs: "+ReservationForm.A.getNb_voyageur());
        
        
        
        
        
        
       
       LA8.setText("Type_Carte: "+ReservationForm.A.getTypecarte());
       LA1.setText("Numéro_Carte: "+ReservationForm.A.getNumcarte());
         
        
              
              
               
           
               
          BT3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                
                ConnectionRequest cr=new ConnectionRequest()//gérer les req
              { Map data;
       @Override 
      protected void postResponse() { 
          
          
          Dialog.show("",(String)data.get("reponse"), "Ok", null);
          ListReservationForm reservation = new ListReservationForm(theme);
         ///Maryem
           ConnectionRequest CRMail=new ConnectionRequest()//
              { Map <String ,Object> data1;
       @Override 
      protected void postResponse() { 
          
          java.util.List<Map<String,Object>> content =(java.util.List<Map<String,Object>>)data1.get("root");
          
          for(Map<String,Object> obj :content){
             
                
              
              try {
                  DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                  
                  Date date1 = (Date)formatter.parse((String)obj.get("dateDebut"));
                  Date date2 = (Date)formatter.parse((String)obj.get("datefin"));
                  int difference= ((int)((date2.getTime()/(24*60*60*1000))-(int)(date1.getTime()/(24*60*60*1000))));
                 
                  Message m = new Message("Cher client \n"
                          + "Félicitations  vous avez réservé un "+(String)obj.get("type_habitation")+" à "+(String)obj.get("pays")+" pour une \n"
                          + " période qui dure "+difference+"jours de "+((String)obj.get("dateDebut")).substring(0, 10)+" jusqu'à "+(String)obj.get("datefin")+"! \n"
                          
                          + ""
                          + "Bon séjour !\n"
                          + "Cdt,\n"
                          + "HostAndGuest\n"
                  );
                  
                  
                  Display.getInstance().sendMessage(new String[] {ReservationForm.A.getEmail()}, "Bon de réservation du logement choisi ", m);
              } catch (ParseException ex) {
                  
              }
          
           
                
              
              
            
          }
            
          
          
          
          
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
       JSONParser parser=new JSONParser(); 
        
       data1=parser.parseJSON(new InputStreamReader(input));
        
          
      
} };
          ///Maryem
          CRMail.setUrl("http://localhost/mobile/logementrech.php");
          
            CRMail.addArgument("id_log", ""+ReservationForm.A.getId_log());
                NetworkManager.getInstance().addToQueue(CRMail);
          
      }
       @Override 
       protected void readResponse(InputStream input) throws IOException {
       JSONParser parser=new JSONParser(); 
       data=parser.parseJSON(new InputStreamReader(input));
      
} };
                
                
           
        
        
              cr.setUrl("http://localhost/mobile/confirmerreservation.php");
                System.out.println("Mar"+ReservationForm.A.getId_res());
              cr.addArgument("id_res", ""+ReservationForm.A.getId_res());
              
                
             
              
                
             
             
               NetworkManager.getInstance().addToQueue(cr);//executer les req
               InfiniteProgress prog=new  InfiniteProgress();
               Dialog d = prog.showInifiniteBlocking();
               cr.setDisposeOnCompletion(d);
              
                
                
                
                
              
            }
        });
        
 }

   

 
    public Form getF() {
        return fm;
    }

    public void setF(Form f) {
        this.fm = f;
    }
    
    
    
 public void show(){
     
      fm.show(); 
 }
}

