/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

/**
 *
 * @author Mounq
 */
public class feedback {
      
    private int user_id;
    private int rating ; 

    public int getUser_id() {
        return user_id;
    }

    public feedback(int user_id, int rating, String date, String Message) {
        this.user_id = user_id;
        this.rating = rating;
        this.date = date;
        this.Message = Message;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public feedback() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
    private String date ; 
    private String Message ;
    
   
}
