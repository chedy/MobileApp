/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.entite;

import java.util.List;

/**
 *
 * @author Mimoo
 */
public class Logement {
    
    private int id;
    private String type_chambre;
    private String nb_voyageur;
    private String type_habitation;
    private String nb_lit;
    private String type_lit;
    private String nb_SalleBain;
    private String type_SB;
    private String equipement;
    private String espace_utilisé;
    private String prix;
    private String nb_points;
    private String description;
    private String note;
    private String brochure;
    private int idProp;
    private String dateDebut;
    private String dateFin;   
    private String adressgmap;
    private String latlang;
    private String titre;
    private String region;
    private String pays;
    private String brochure1;
    private String brochure2;
    private String brochure3;  
    
    public Logement() {}
   
    public Logement(int id,String type_chambre, String nb_voyageur, String type_habitation, String nb_lit, String type_lit, String nb_SalleBain, String type_SB, String equipement, String espace_utilisé, String prix, String nb_points, String description, String note, String brochure, int idProp, String dateDebut, String dateFin, String adressgmap, String latlang, String titre, String region, String pays, String brochure1, String brochure2, String brochure3) {
        this.id = id;
        this.type_chambre = type_chambre;
        this.nb_voyageur = nb_voyageur;
        this.type_habitation = type_habitation;
        this.nb_lit = nb_lit;
        this.type_lit = type_lit;
        this.nb_SalleBain = nb_SalleBain;
        this.type_SB = type_SB;
        this.equipement = equipement;
        this.espace_utilisé = espace_utilisé;
        this.prix = prix;
        this.nb_points = nb_points;
        this.description = description;
        this.note = note;
        this.brochure = brochure;
        this.idProp = idProp;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.adressgmap = adressgmap;
        this.latlang = latlang;
        this.titre = titre;
        this.region = region;
        this.pays = pays;
        this.brochure1 = brochure1;
        this.brochure2 = brochure2;
        this.brochure3 = brochure3;
    }
   
   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType_chambre() {
        return type_chambre;
    }

    public void setType_chambre(String type_chambre) {
        this.type_chambre = type_chambre;
    }

    public String getNb_voyageur() {
        return nb_voyageur;
    }

    public void setNb_voyageur(String nb_voyageur) {
        this.nb_voyageur = nb_voyageur;
    }

    public String getNb_lit() {
        return nb_lit;
    }

    public void setNb_lit(String nb_lit) {
        this.nb_lit = nb_lit;
    }

  
    public String getType_habitation() {
        return type_habitation;
    }

    public void setType_habitation(String type_habitation) {
        this.type_habitation = type_habitation;
    }



    public String getType_lit() {
        return type_lit;
    }

    public void setType_lit(String type_lit) {
        this.type_lit = type_lit;
    }


    public String getType_SB() {
        return type_SB;
    }

    public void setType_SB(String type_SB) {
        this.type_SB = type_SB;
    }

    public String getEquipement() {
        return equipement;
    }

    public void setEquipement(String equipement) {
        this.equipement = equipement;
    }

    public String getEspace_utilisé() {
        return espace_utilisé;
    }

    public void setEspace_utilisé(String espace_utilisé) {
        this.espace_utilisé = espace_utilisé;
    }

 

    public void setPrix(Double prix) {
        this.setPrix((double) prix);
    }

  

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBrochure() {
        return brochure;
    }

    public void setBrochure(String brochure) {
        this.brochure = brochure;
    }

    public int getIdProp() {
        return idProp;
    }

    public void setIdProp(int idProp) {
        this.idProp = idProp;
    }

  
    public String getAdressgmap() {
        return adressgmap;
    }

    public void setAdressgmap(String adressgmap) {
        this.adressgmap = adressgmap;
    }

    public String getLatlang() {
        return latlang;
    }

    public void setLatlang(String latlang) {
        this.latlang = latlang;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getBrochure1() {
        return brochure1;
    }

    public void setBrochure1(String brochure1) {
        this.brochure1 = brochure1;
    }

    public String getBrochure2() {
        return brochure2;
    }

    public void setBrochure2(String brochure2) {
        this.brochure2 = brochure2;
    }

    public String getBrochure3() {
        return brochure3;
    }

    public void setBrochure3(String brochure3) {
        this.brochure3 = brochure3;
    }

    public String getNb_SalleBain() {
        return nb_SalleBain;
    }

    public void setNb_SalleBain(String nb_SalleBain) {
        this.nb_SalleBain = nb_SalleBain;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getNb_points() {
        return nb_points;
    }

    public void setNb_points(String nb_points) {
        this.nb_points = nb_points;
    }


    /**
     * @return the dateDebut
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * @param dateDebut the dateDebut to set
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * @return the dateFin
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     * @param dateFin the dateFin to set
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }
  @Override
    public String toString() {
        return titre + "\n" +adressgmap;
    }
  
}